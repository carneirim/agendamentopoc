package org.fleury.schedulingPOC;

import org.fleury.schedulingPOC.domain.SchedulingSolution;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.core.impl.score.director.ScoreDirectorFactory;

public class SchedulingApp {
	public static final String SOLVER_CONFIG_XML = "org/fleury/schedulingPOC/solver/schedulingSolverConfig.xml";
	public static ScoreDirector scoreDirector;
	public static int printCount = 0;

	public static void main(String[] args) {
		Solver solver = SolverFactory.createFromXmlResource(SOLVER_CONFIG_XML)
				.buildSolver();
		ScoreDirectorFactory scoreDirectorFactory = solver
				.getScoreDirectorFactory();
		scoreDirector = scoreDirectorFactory.buildScoreDirector();
		SchedulingSolution schedulingSolution = new SchedulingSolution();
		scoreDirector.setWorkingSolution(schedulingSolution);
		System.out.println("\n\nTRYING COUNT AT:");
		solver.solve(schedulingSolution);
		SchedulingSolution bestSchedulingSolution = (SchedulingSolution) solver
				.getBestSolution();
		System.out
				.println("\n\nBEST SOLUTION: "
						+ bestSchedulingSolution.toString());
	}

}
