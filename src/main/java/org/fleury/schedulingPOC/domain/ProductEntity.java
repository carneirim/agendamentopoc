package org.fleury.schedulingPOC.domain;

public class ProductEntity {
	private String name;

	public ProductEntity(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString () {
		return this.name;
	}
}
