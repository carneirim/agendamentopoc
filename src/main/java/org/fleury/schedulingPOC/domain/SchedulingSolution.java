package org.fleury.schedulingPOC.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.fleury.schedulingPOC.SchedulingApp;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

@PlanningSolution
public class SchedulingSolution implements Solution<HardSoftScore>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1416027214755223706L;
	
	private List<BuildingEntity> buildings;
	private List<CustomerEntity> customers;
	private List<CustomerAssignmentEntity> customerAssignments;
//	private List<ProductEntity> products;
	
	private HardSoftScore score;

	public SchedulingSolution() {
		populateBuildings();
		populateCustomers();
//		populateProducts();
		populateCustomerAssignments();
	}
	
	public Collection<? extends Object> getProblemFacts() {
		List<Object> facts = new ArrayList<Object>();
		facts.addAll(customers);
//		facts.addAll(products);
		facts.addAll(buildings);
		// nothing to add because the only facts are already added automatically
		// by planner
		return facts;
	}

	public HardSoftScore getScore() {
		if(null==SchedulingApp.scoreDirector){
			System.out.println("WOOPS YOU SHOULD EXPECT SOME ISSUES HERE");
		}
		HardSoftScore hardSoftScore = (HardSoftScore)SchedulingApp.scoreDirector.calculateScore();
		System.out.println("SCORE "+ hardSoftScore.toString() );
		return hardSoftScore;
	}

	public void setScore(HardSoftScore score) {
		this.score = score;
	}

    @ValueRangeProvider(id = "buildingRange")
	public List<BuildingEntity> getBuildings() {
		return buildings;
	}

    public void setBuildings(List<BuildingEntity> buildings) {
		this.buildings = buildings;
	}

	public List<CustomerEntity> getCustomers() {
		return customers;
	}

	public void setCustomers(List<CustomerEntity> customers) {
		this.customers = customers;
	}

	@PlanningEntityCollectionProperty
	public List<CustomerAssignmentEntity> getCustomerAssignments() {
		return customerAssignments;
	}

	public void setCustomerAssignments(List<CustomerAssignmentEntity> customerAssignments) {
		this.customerAssignments = customerAssignments;
	}
	
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		
		for(CustomerAssignmentEntity entity : customerAssignments) {
			buffer.append(entity.toString());
		}
		
		if (buffer.toString().length() == 0) buffer.append("Nada");
		
		return buffer.toString();
	}

//    @ValueRangeProvider(id = "productRange")
//    public List<ProductEntity> getProducts() {
//		return products;
//	}
//
//	public void setProducts(List<ProductEntity> products) {
//		this.products = products;
//	}
	
	private void populateBuildings() {
		if (null == buildings) {
			buildings = new ArrayList<BuildingEntity>(8);
			buildings.add(new BuildingEntity("M", "Norte", 3.8));
			buildings.add(new BuildingEntity("M", "Leste", 2));
			buildings.add(new BuildingEntity("M", "Sul", 1.5));
			buildings.add(new BuildingEntity("M", "Oeste", 2));
			buildings.add(new BuildingEntity("F", "Norte", 2));
			buildings.add(new BuildingEntity("F", "Leste", 2));
			buildings.add(new BuildingEntity("F", "Sul", 2));
			buildings.add(new BuildingEntity("F", "Oeste", 2));
		}
	}
	
	private void populateCustomerAssignments() {
		if (null == customerAssignments) {
			customerAssignments = new ArrayList<CustomerAssignmentEntity>(1);
			for (CustomerEntity customer : customers) {
//				for (ProductEntity product : products) {
					CustomerAssignmentEntity ett = new CustomerAssignmentEntity();
					ett.setCustomer(customer);
//					ett.setProduct(product);
					customerAssignments.add(ett);
//				}
			}
		}
	}
	
//	private void populateProducts() {
//		if (null == products) {
//			products = new ArrayList<ProductEntity>();
//			products.add(new ProductEntity("HT"));
//			products.add(new ProductEntity("TERGO"));
//			products.add(new ProductEntity("GLIC"));
//			products.add(new ProductEntity("HIV"));
//		}
//	}
	
	private void populateCustomers() {
		if (null == customers) {
			customers = new ArrayList<CustomerEntity>(1);
			customers.add(new CustomerEntity("M", "Sul", 3));
		}
	}
}
