package org.fleury.schedulingPOC.domain;

public class BuildingEntity {
	
	private String sex;
	private String region;
	private double maxHeight;
	
	public BuildingEntity(String sex, String region, double maxHeight) {
		this.sex = sex;
		this.region = region;
		this.maxHeight = maxHeight;
	}
	
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	public String toString () {
		return "Sex: "+ this.sex + " - Region: " + this.region;
	}

	public double getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(double maxHeight) {
		this.maxHeight = maxHeight;
	}

}
