package org.fleury.schedulingPOC.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
public class CustomerAssignmentEntity {
	private BuildingEntity building;
	private CustomerEntity customer;
//	private ProductEntity product;
	
    @PlanningVariable(valueRangeProviderRefs = {"buildingRange"})
	public BuildingEntity getBuilding() {
		return building;
	}
	public void setBuilding(BuildingEntity building) {
		this.building = building;
	}
	
	public CustomerEntity getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}
	
	public String getCustomerSex() {
		String returnValue = null;
		if (customer != null) {
			returnValue = customer.getSex();
		}
		return returnValue;
	}
	
	public String getBuildingSex() {
		String returnValue = null;
		if (building != null) {
			returnValue = building.getSex();
		}
		return returnValue;
	}
	
	public String getCustomerRegion() {
		String returnValue = null;
		if (customer != null) {
			returnValue = customer.getRegion();
		}
		return returnValue;
	}
	
	public String getBuildingRegion() {
		String returnValue = null;
		if (building != null) {
			returnValue = building.getRegion();
		}
		return returnValue;
	}
	
	public double getCustomerHeight() {
		double returnValue = 0;
		if (customer != null) {
			returnValue = customer.getHeight();
		}
		return returnValue;
	}
	
	public double getBuildingMaxHeight() {
		double returnValue = 0;
		if (building != null) {
			returnValue = building.getMaxHeight();
		}
		return returnValue;
	}
	
	public String toString() {
		return "Customer: [" + (this.customer == null ? "" : this.customer.toString()) + "] - Building: [" + (this.building == null ? "" : this.building.toString()) + "]";
	}

//    @PlanningVariable(valueRangeProviderRefs = {"productRange"})
//	public ProductEntity getProduct() {
//		return product;
//	}

//	public void setProduct(ProductEntity product) {
//		this.product = product;
//	}
	
}
