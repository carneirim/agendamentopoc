package org.fleury.schedulingPOC.domain;

public class CustomerEntity {
	private String sex;
	private String region;
	private double height;
	
	public CustomerEntity (String sex, String region, double height) {
		this.sex = sex;
		this.region = region;
		this.height = height;
	}
	
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	public String toString() {
		return "Sex: " + this.sex + " - Region: " + this.region;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
	
}
